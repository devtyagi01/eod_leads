import os
import boto3
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

SENDER = "dev@tartanhq.com"
SUBJECT = "Leads from Batik"
CHARSET = "UTF-8"
AWS_REGION = "us-east-2"
client = boto3.client('ses',region_name=AWS_REGION)
ATTACHMENT = "output.csv"
BODY_TEXT = "Here are the leads from Batik, Please find the attached CSV file."


def send_lead(provider_email):
    RECIPIENT = provider_email

    msg = MIMEMultipart('mixed')
    msg['Subject'] = SUBJECT 
    msg['From'] = SENDER 
    msg['To'] = RECIPIENT
    msg['Cc'] = "arnav@tartanhq.com, pratikdesai@tartanhq.com, dev@tartanhq.com"

    msg_body = MIMEMultipart('alternative')
    textpart = MIMEText(BODY_TEXT.encode(CHARSET), 'plain', CHARSET)

    msg_body.attach(textpart)
    att = MIMEApplication(open(ATTACHMENT, 'rb').read())
    att.add_header('Content-Disposition','attachment',filename=os.path.basename(ATTACHMENT))

    msg.attach(msg_body)
    msg.attach(att)

    try:
        #Provide the contents of the email.
        response = client.send_raw_email(
            Source=SENDER,
            Destinations=[
                RECIPIENT,
                "arnav@tartanhq.com", 
                "pratikdesai@tartanhq.com", 
                "dev@tartanhq.com"
            ],
            RawMessage={
                'Data':msg.as_string(),
            }
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])