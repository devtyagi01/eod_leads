import pymysql
from pymysql import cursors
from json_to_csv import *

db = pymysql.connect(host='localhost', user='root', password='12345678', db='scrapped_data')

cursor = db.cursor()
cursor.execute("select distinct(provider_email) from scrapped_data where availed_date = curdate()")
output = cursor.fetchall()

for i in output:
    email = i[0]
    cursor.execute(f"select sync_id, metadata, availed_date, availed_time, benefit_name, provider_email from scrapped_data where provider_email = '{email}' and availed_date = curdate()")
    data = cursor.fetchall()
    process_data(data, email)