import json
from send_mail import *

def get_csv_row(row, row_num=0):
    if(row_num == 0):
        json_data = json.loads(row[1])
        availed_date = str(row[2])
        availed_time = str(row[3])
        benefit_name = row[4]
        customer_name = json_data['customerSummary']['name']
        employer_name = json_data['customerSummary']['employerName']
        employee_id = json_data['customerSummary']['employeeId']
        income_details = json_data['incomeDetails']['incomes'][row_num]
        income_year = str(income_details['year'])
        income_month = str(income_details['month'])
        month_total_earning = str(income_details['totalEarnings'])
        month_total_deduction = str(income_details['totalDeductions']) 
        month_net_pay = str(income_details['netPay'])
        return availed_time + "," + availed_date + "," + benefit_name + "," + customer_name + "," + employer_name + "," + employee_id + "," + income_year + "," + income_month + "," + month_total_earning + "," + month_total_deduction + "," + month_net_pay
    else:
        json_data = json.loads(row[1])
        income_details = json_data['incomeDetails']['incomes'][row_num]
        income_year = str(income_details['year'])
        income_month = str(income_details['month'])
        month_total_earning = str(income_details['totalEarnings'])
        month_total_deduction = str(income_details['totalDeductions']) 
        month_net_pay = str(income_details['netPay'])
        return ",,,,,," + income_year + "," + income_month + "," + month_total_earning + "," + month_total_deduction + "," + month_net_pay

def get_csv(row):
    csv_data = ''
    json_data = json.loads(row[1])
    num_rows = len(json_data['incomeDetails']['incomes'])
    for i in range(num_rows):
        csv_data += get_csv_row(row, i) + '\n'
    return csv_data
    
csv_cols = 'availed_time,availed_date,benefit_name,customer_name,employer_name, employee_id,income_year,income_month,month_total_earning,month_total_deduction,month_net_pay\n'

def process_data(data, email):
    file = open('output.csv', 'a')
    file.truncate(0)
    file.write(csv_cols)
    for row in data:
        csv = get_csv(row)
        file.write(csv)
    file.close()
    send_lead(email)


    
